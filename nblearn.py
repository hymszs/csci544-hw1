import sys
import os
import json
inputpath=sys.argv[1]
outputpath=sys.argv[2]
g=open(outputpath,'w')
f=open(inputpath,'r',encoding = "ISO-8859-1")
dict_total={}
line=f.readline()
lll=line.split(' ')
if(lll[0]=="SPAM" or lll[0]=="HAM"):
	f.seek(0,0)
	spam_word_count=0
	ham_word_count=0
	total_word_count=0
	isSpam=0
	dict_spam= {}
	dict_ham={}
	while True:
		line=f.readline()
		if len(line)==0:
			break
		wordlist=line.split(' ')
		if(wordlist[0]=="SPAM"):
			isSpam=1
		if(wordlist[0]=="HAM"):
			isSpam=0
		length=len(wordlist)
		for i in range(0,length):
			if(wordlist[i].isalpha()==True and wordlist[i]!="Subject:"):
				if(isSpam==1):
					spam_word_count=spam_word_count+1
					if(wordlist[i] in dict_spam):
						dict_spam[wordlist[i]]+=1
					else:
						dict_spam[wordlist[i]]=1
				else:
					ham_word_count=ham_word_count+1
					if(wordlist[i] in dict_ham):
						dict_ham[wordlist[i]]+=1
					else:
						dict_ham[wordlist[i]]=1
#dict_ham['subject']-=13545
#dict_spam['subject']-=4912
#spam_word_count-=4912
#ham_word_count-=13545
	dict_ham["ham_file_count"]=13545
	dict_spam["spam_file_count"]=4912
	dict_ham["ham_word_count"]=ham_word_count
	dict_spam["spam_word_count"]=spam_word_count
	dict_total["SPAM"]=dict_spam
	dict_total["HAM"]=dict_ham
	#total_word_count=spam_word_count+ham_word_count
	#g.write(str(4912))
	#g.write("\n")
	#g.write(str(13545))
	#g.write("\n")
	#g.write(str(spam_word_count))
	#g.write("\n")
	#g.write(str(ham_word_count))
	#g.write("\n")
	#g.write(str(total_word_count))
	#g.write("\n")
	#g.write(" \n")
	#for k in dict_spam.keys():
		#g.write(k)
		#g.write(" ")
		#g.write(str(dict_spam[k]))
		#g.write("\n")
	#g.write(" \n")
	#for k in dict_ham.keys():
		#g.write(k)
		#g.write(" ")
		#g.write(str(dict_ham[k]))
		#g.write("\n")
	#f.close()
	#g.close()
else:
	f.seek(0,0)
	neg_word_count=0
	pos_word_count=0
	total_word_count=0
	isneg=0
	dict_neg= {}
	dict_pos={}
	while True:
		line=f.readline()
		if len(line)==0:
			break
		wordlist=line.split(' ')
		if(wordlist[0]=="NEG"):
			isneg=1
		if(wordlist[0]=="POS"):
			isneg=0
		length=len(wordlist)
		for i in range(0,length):
			if(wordlist[i].isalpha()==True):
				if(isneg==1):
					neg_word_count=neg_word_count+1
					if(wordlist[i] in dict_neg):
						dict_neg[wordlist[i]]+=1
					else:
						dict_neg[wordlist[i]]=1
				else:
					pos_word_count=pos_word_count+1
					if(wordlist[i] in dict_pos):
						dict_pos[wordlist[i]]+=1
					else:
						dict_pos[wordlist[i]]=1
#dict_ham['subject']-=13545
#dict_spam['subject']-=4912
#spam_word_count-=4912
#ham_word_count-=13545
	dict_pos["pos_file_count"]=12500
	dict_neg["neg_file_count"]=12500
	dict_pos["pos_word_count"]=pos_word_count
	dict_neg["neg_word_count"]=neg_word_count
	dict_total["POS"]=dict_pos
	dict_total["NEG"]=dict_neg
	#total_word_count=pos_word_count+neg_word_count
	#g.write(str(12500))
	#g.write("\n")
	#g.write(str(12500))
	#g.write("\n")
	#g.write(str(neg_word_count))
	#g.write("\n")
	#g.write(str(pos_word_count))
	#g.write("\n")
	#g.write(str(total_word_count))
	#g.write("\n")
	#g.write("NEG\n")
	#for k in dict_neg.keys():
		#g.write(k)
		#g.write(" ")
		#g.write(str(dict_neg[k]))
		#g.write("\n")
	#g.write("POS\n")
	#for k in dict_pos.keys():
		#g.write(k)
		#g.write(" ")
		#g.write(str(dict_pos[k]))
		#g.write("\n")
f.close()
json.dump(dict_total, open(outputpath,'w'))