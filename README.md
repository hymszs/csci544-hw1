part I
SPAM dataset:
	precision:0.988
	recall:0.947
	Fscore:0.967
SENTIMENT dataset:
	precision:0.89
	recall:0.72
	Fscore:0.80
part II

SVM
SPAM dataset:
	precision:0.76
	recall:0.95
	Fscore:0.85
SENTIMENT dataset:
	precision:0.79
	recall:0.84
	Fscore:0.81

MEGAM
SPAM dataset:
	precision:0.87
	recall:0.94
	Fscore:0.90
SENTIMENT dataset:
	precision:0.82
	recall:0.76
	Fscore:0.79

PART III
10% SPAM dataset:
	precision:0.96
	recall:0.15
	Fscore:0.26
10% SENTIMENT dataset:
	precision:0.72
	recall:0.75
	Fscore:0.85
WHY: the spam dataset I selected seems to have high bias while predicting.It maybe because I select too more spam data.For the sentiment data I think the Fscore is a little lower since I use less dataset for training.