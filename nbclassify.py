import sys
import os
import math
import json
modelpath=sys.argv[1]
textpath=sys.argv[2]
dict_total={}
dict_spam= {}
dict_ham={}
dict_total=json.load(open(modelpath))
classifier_type=0
if("SPAM" in dict_total.keys()):
	dict_spam=dict_total["SPAM"]
	dict_ham=dict_total["HAM"]
	classifier_type=1
	c1_name="SPAM"
	c2_name="HAM"
	spam_file_count=dict_spam["spam_file_count"]
	ham_file_count=dict_ham["ham_file_count"]
	spam_word_count=dict_spam["spam_word_count"]
	ham_word_count=dict_ham["ham_word_count"]
else:
	dict_spam=dict_total["NEG"]
	dict_ham=dict_total["POS"]
	classifier_type=2
	c1_name="NEG"
	c2_name="POS"
	spam_file_count=dict_spam["neg_file_count"]
	ham_file_count=dict_ham["pos_file_count"]
	spam_word_count=dict_spam["neg_word_count"]
	ham_word_count=dict_ham["pos_word_count"]
#total_word_count=dict_spam["spam_file_count"]
#if("spam" in modelpath):
#	c2_idx=63706
#	c1_name="SPAM"
#	c2_name="HAM"
#	classifier_type=1
#else:
#	c2_idx=54649
#	c1_name="NEG"
#	c2_name="POS"
#	classifier_type=2
#f=open(modelpath,'r',encoding = "ISO-8859-1")


#f.readline()
#isSpam=1;
#count=7
#while True:
#	line=f.readline()
#	if len(line)==0:
#		break
#	wordlist=line.split(' ')
#	if(count==c2_idx):
#		isSpam=0
#	if(len(wordlist)>1 and count!=c2_idx):
#		if(isSpam==1):
#			dict_spam[wordlist[0]]=float(wordlist[1])
#		else:
#			dict_ham[wordlist[0]]=float(wordlist[1])
#	count=count+1
#f.close()
	#start classify
#print(len(dict_spam))
#print(len(dict_ham))
aa=0
ab=0
ba=0
bb=0
g=open(textpath,'r')
if(classifier_type==1):
	h=open("spam.out",'w')
if(classifier_type==2):
	h=open("sentiment.out",'w')
while True:
	line=g.readline()
	if len(line)==0:
		break
	wordlist=line.split(' ')
	start_point=1
	#length=len(wordlist)
	a=math.log(float(spam_file_count)/(float(spam_file_count)+float(ham_file_count)))
	b=math.log(float(ham_file_count)/(float(spam_file_count)+float(ham_file_count)))
	for i in range(start_point,len(wordlist)):
		if(wordlist[i].isalpha()==True):				
		#	a*=dict_spam[wordlist[i]]/spam_word_count
		#	b*=dict_ham[wordlist[i]]/ham_word_count
			if(wordlist[i] in dict_spam):
				p1=float(dict_spam[wordlist[i]]+1)/float(spam_word_count+len(wordlist))
			else:
				p1=float(1)/float(spam_word_count+len(wordlist))
			if(wordlist[i] in dict_ham):
				p2=float(dict_ham[wordlist[i]]+1)/float(ham_word_count+len(wordlist))
			else:
				p2=float(1)/float(ham_word_count+len(wordlist))
			a+=math.log(p1)
			b+=math.log(p2)	
			#print(str(p1))
			#print(str(p2))	
	if(a>b):
		h.write(c1_name)
		h.write("\n")
	else:
		h.write(c2_name)
		h.write("\n")
g.close()
h.close()