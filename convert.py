import sys
import os
inputpath=sys.argv[1]
outputpath=sys.argv[2]
g=open(outputpath,'w')
fname=[];
for filename in os.listdir(inputpath):
	fname.append(filename)
fname.sort()
for filename in fname:
	filepath=os.path.join(inputpath,filename)
	f=open(filepath,'r',encoding = "ISO-8859-1")
	if(filename[0:1])=="S":
		g.write("SPAM ")
	if(filename[0:1])=="H":
		g.write("HAM ")
	if(filename[0:1])=="N":
		g.write("NEG ")
	if(filename[0:1])=="P":
		g.write("POS ")
	while True:
		line=f.readline()
		if len(line)==0:
			break
		g.write(line[:-1])
		g.write(" ")
	g.write("\n")	
	f.close()
g.close()
#python3 convert.py /home/zsure/Documents/nlp/hw1/SPAM_training /home/zsure/Documents/nlp/hw1/testoutput/output.txt 

